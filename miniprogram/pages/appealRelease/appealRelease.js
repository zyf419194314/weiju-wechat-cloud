import {
  appealSave,
  appealTag,
  uploadSecurityImg
} from "../../api/index"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: true,

    imageArray: [], // 图片
    defaultImg: 'https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/channels4_banner.jpg',
    imgMode: 'aspectFit',
    title: '',
    content: '',
    latitude: '',
    longitude: '',

    // 主要展示用户
    addressHint: '一定要选择距离自己较远的位置',
    tagList: []


  },
  pageData: {
    imageModeArray: ["scaleToFill", "aspectFit", "aspectFill", "widthFix", "heightFix",
      "top", "bottom", "center", "left", "right", "top left", "top right", "bottom left", "bottom right"
    ] // 图片展示的方式
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.queryAppealTag()

  },

  return (e) {
    wx.navigateBack({
      delta: 2
    })
  },

  // 选择图片
  selectImg: async function() {
    let imgArray = this.data.imageArray
    if (imgArray.length > 4) {
      return wx.showToast({
        title: '图片够多啦',
        icon: 'none'
      })
      
    }
    const result = await uploadSecurityImg({count:4})
    if (imgArray.length + result.length > 4) {
      return wx.showToast({
        title: '图片太多啦',
        icon: 'none'
      })
    }
    if(result.length!==0){
      let uploadArr = result.map(item=>item.file)
      this.setData({
        imageArray:imgArray.concat(uploadArr),
        defaultImg:uploadArr[uploadArr.length-1]
      })
    }
  },

  // 点击图片 切换背景图片
  clickImg(e) {
    let index = e.currentTarget.dataset.index
    let imageArray = this.data.imageArray
    this.setData({
      defaultImg: imageArray[index]
    })
  },

  // 记录输入的标题
  inputTitle(e) {

    console.log(e.detail.value)
    this.setData({
      title: e.detail.value
    })

  },

  // 记录输入的内容
  inputContent(e) {

    this.setData({
      content: e.detail.value
    })

  },

  // 上传图片
  uploadAppealImg() {

    return new Promise((resolve, reject) => {

      let uploadImgArr = []
      let uploadIndex = 0
      let imgArr = this.data.imageArray

      imgArr.forEach(item => {

        wx.cloud.uploadFile({
          cloudPath: `appeal/${Date.now()}-${Math.floor(Math.random(0, 1) * 1000)}` + item.match(/\.[^.]+?$/),
          filePath: item, // 文件路径
        }).then(res => {

          uploadIndex++
          uploadImgArr.push(res.fileID)

          //判断是否全部上传完成
          if (imgArr.length === uploadIndex) {
            resolve(uploadImgArr)
          }

        }).catch(error => {
          wx.hideLoading()
          console.log(error)
        })


      })




    })

  },


  // 保存诉求
  saveAppealOrUploadImg: async function () {
    if(this.data.title===''){
      return wx.showToast({
        title: '标题不能为空',
        icon:"none"
      })
    }
    if(this.data.content===''){
      return wx.showToast({
        title: '诉求内容不能为空',
        icon:"none"
      })
    }
    wx.showLoading({
      title: '上传中😍',
    })

    let imgArray = []
    if(this.data.imageArray.length !==0 ){
      imgArray = await this.uploadAppealImg();
    }
    

    // 诉求标签
    let appealTag = []
    this.data.tagList.forEach(item => {
      if(item.check){
        appealTag.push(item)
      }
    })

    let data = {
      title: this.data.title,
      content: this.data.content,
      latitude: this.data.latitude,
      longitude: this.data.longitude,
      appealMaterial: imgArray,
      appealTag: appealTag
    }

    let result = await appealSave(data)

    if (result._id) {

      wx.showToast({
        title: '保存成功',
      })

      wx.hideLoading()
      this.clearData()

      setTimeout(() => {
        wx.navigateBack({
          delta: 1
        })
      }, 1000)

    } else {

      wx.showToast({
        title: '保存失败',
        icon: 'none'
      })

    }



  },

  async queryAppealTag(){

    let result = await appealTag();
    this.setData({
      tagList: result || []
    })

  },

  handleTag(e){

    let index = e.currentTarget.dataset.index
    
    let tagList = this.data.tagList
    tagList[index].check = tagList[index].check ? false : true

    this.setData({
      tagList: tagList
    })


  },


  // 打开地图选择位置
  handleOpenMap() {

    let that = this

    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success(res) {
        let latitude = res.latitude
        let longitude = res.longitude

        wx.chooseLocation({
          latitude,
          longitude,
          success: (result) => {

            let lat = result.latitude
            let log = result.longitude
            that.setData({
              latitude: lat,
              longitude: log,
              addressHint: result.name
            })

          },
        })
      }
    })

  },

  // 清空数据
  clearData() {
    this.setData({
      imageArray: [],
      title: '',
      content: '',
      latitude: '',
      longitude: '',
      addressHint: '一定要选择距离自己较远的位置'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})