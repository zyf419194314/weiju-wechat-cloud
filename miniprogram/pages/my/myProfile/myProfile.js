const app = getApp()
import {
  getUserInfo
} from "../../../api/index"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    wjUser: {}
  },
  pageData: {
    pageNO: 1,
    pageSize: 10
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let wjUser = wx.getStorageSync('wjUser')
    if (!wjUser) {
      wx.navigateTo({
        url: '/pages/welcome/welcome',
      })
      return
    }
    this.setData({
      wjUser: wjUser
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getUserInfo()
  },
  // 去编辑页面
  gotoEditProfile() {
    wx.navigateTo({
      url: '/pages/my/editProfile/editProfile',
    })
  },
  gotoUserAppeal:function(){
    wx.navigateTo({
      url: '/pages/my/myAppeal/myAppeal',
    })
  },
  gotoUserDynamic:function(){
    wx.navigateTo({
      url: '/pages/my/mydynamic/mydynamic',
    })
  },
  getUserInfo: async function () {
    const result = await getUserInfo({})
    if (result) {
      this.setData({
        wjUser: result
      })
    }
  },
  gotoUserComment:function(){
    wx.navigateTo({
      url: '/pages/my/myComment/myComment',
    })
  }
})