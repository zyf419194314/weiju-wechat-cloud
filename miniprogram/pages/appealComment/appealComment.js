import util from '../../utils/util.js';
import {
  pageAppealComment,
  appealPutComment
} from "../../api/index"
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    appealId: '',
    commentList: [],
    comment: '' 
  },

  pageData: {
    pageNO: 1,
    pageSize: 10
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    let appealId = options.appealId

    this.setData({
      appealId: appealId
    })

    this.queryComment();

  },

  // 查询评论
  queryComment: async function () {

    let data = {
      appealId: this.data.appealId,
      pageNO: this.pageData.pageNO,
      pageSize: this.pageData.pageSize
    }

    let commentList = await pageAppealComment(data);

    commentList.forEach(item => {
      item.createTime = util.format(new Date(item.createTime))
    })

    this.setData({
      commentList: commentList
    })


  },

  // 监控评论
  monitorComment(e) {

    let comment = e.detail.value

    this.setData({
      comment: comment
    })

  },

  // 发送评论
  putComment: async function () {

    let comment = this.data.comment

    let newComment = comment.replace(/\s+/g, "")
    if (!newComment) {
      return
    }

    let data = {
      appealId: this.data.appeal._id,
      content: comment,
    }

    let result = await appealPutComment(data);

    if (result._id) {
      this.setData({
        comment: ''
      })
      wx.showToast({
        title: '评论成功',
        icon:"none"
      })
      
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})